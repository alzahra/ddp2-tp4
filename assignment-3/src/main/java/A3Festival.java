import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;

public class A3Festival {
    // Check if the three required csv files exist in the folder
    private static boolean checkFiles(File[] files) {
        int count = 0;
        for (File f: files) {
            if (f.getName().equalsIgnoreCase("animals_categories.csv")) {
                count += 1;
            } else if (f.getName().equalsIgnoreCase("animals_attractions.csv")) {
                count += 1;
            } else if (f.getName().equalsIgnoreCase("animals_records.csv")) {
                count += 1;
            }
        }
        return (count == 3);
    }

    // Store acquired csv files
    private static File[] makeFiles(File[] files) {
        File[] csvFiles = new File[3];
        for (File f: files) {
            if (f.getName().equalsIgnoreCase("animals_categories.csv")) {
                csvFiles[0] = f;
            } else if (f.getName().equalsIgnoreCase("animals_attractions.csv")) {
                csvFiles[1] = f;
            } else if (f.getName().equalsIgnoreCase("animals_records.csv")) {
                csvFiles[2] = f;
            }
        }
        return csvFiles;
    }

    // Print the amount of valid and invalid data after reading them through CsvReader
    private static String validData(CategoriesReader category, SectionsReader section, TypeReader type,
                                   AttractionsReader attraction, RecordsReader record) {
        String output = "";
        long validCategory = category.countValidRecords();
        long invalidCategory = category.countInvalidRecords();
        long validSection = section.countValidRecords();
        long invalidSection = section.countInvalidRecords();
        type.checkType(category, section);
        long validAttraction = attraction.countValidRecords();
        long invalidAttraction = attraction.countInvalidRecords();
        long validRecord = record.countValidRecords();
        long invalidRecord = record.countInvalidRecords();
        output += ("\nFound _" + validSection + "_ valid sections and _" + invalidSection
                 + "_ invalid sections");
        output += ("\nFound _" + validAttraction + "_ valid attractions and _" + invalidAttraction
                 + "_ invalid attractions");
        output += ("\nFound _" + validCategory + "_ valid animal categories and _" + invalidCategory
                 + "_ invalid animal categories");
        output += ("\nFound _" + validRecord + "_ valid animal records and _" + invalidRecord
                 + "_ invalid animal records");
        return output;
    }

    // Method to explore the park, user will have to choose a section. This method returns String chosen section
    private static String explorePark() {
        Scanner input = new Scanner(System.in);
        String section = "";
        String output = "";
        output += ("\nJavari Park has " + ValidData.getValidSections().size() + " sections:");
        for (int i = 0; i < ValidData.getValidSections().size(); i++) {
            output += ("\n" + (i + 1) + ". " + ValidData.getValidSections().get(i));
        }
        output += ("\nPlease choose your preferred section (type the number): ");
        System.out.print(output);
        try {
            int choice = input.nextInt();
            if ((choice > 0) && (choice <= ValidData.getValidSections().size())) {
                section = ValidData.getValidSections().get(choice - 1);
            } else {
                return explorePark();
            }
        } catch (Exception e) {
            return explorePark();
        }
        return section;
    }

    // Method to explore a section, user have to choose an animal. This method returns String chosen animal
    private static String exploreSection(String section) {
        Scanner input = new Scanner(System.in);
        String animal = "";
        String output = "";
        output += ("\n--" + section + "--");
        for (int i = 0; i < ValidData.getValidType(section).size(); i++) {
            output += ("\n" + (i + 1) + ". " + ValidData.getValidType(section).get(i));
        }
        output += ("\nPlease choose your preferred animals (type the number): ");
        System.out.print(output);
        try {
            String choice = input.nextLine();
            if (choice.equals("#")) {
                String sect = explorePark();
                return exploreSection(sect);
            } else if (Integer.parseInt(choice) > 0
                && Integer.parseInt(choice) <= ValidData.getValidType(section).size()) {
                animal = ValidData.getValidType(section).get(Integer.parseInt(choice) - 1);
                if (checkAnimals(animal).size() == 0) {
                    System.out.println("\nUnfortunately, no " + animal.toLowerCase() + " can "
                                     + "perform any attraction, please choose other animals");
                    return exploreSection(section);
                }
            } else {
                return exploreSection(section);
            }
        } catch (Exception e) {
            return exploreSection(section);
        }
        return animal;
    }

    // Method to check the availability of all animals in a category to perform attractions
    private static List<Animal> checkAnimals(String type) {
        List<Animal> output = new ArrayList<Animal>();
        for (Animal animal: ValidData.getAnimals()) {
            if (animal.getType().equalsIgnoreCase(type) && animal.isShowable()) {
                output.add(animal);
            }
        }
        return output;
    }

    // Method to choose attractions based on chosen animal. This method returns SelectedAttraction chosen attraction
    private static SelectedAttraction exploreAnimal(String type) {
        Scanner input = new Scanner(System.in);
        String output = "";
        String attraction = "";
        SelectedAttraction selectedAttraction = new Attractions(attraction, type, checkAnimals(type));
        output += ("\n--" + type + "--");
        for (int i = 0; i < ValidData.getAttraction(type).size(); i++) {
            output += ("\n" + (i + 1) + ". " + ValidData.getAttraction(type).get(i));
        }
        output += ("\nPlease choose your preferred attractions (type the number): ");
        System.out.print(output);
        try {
            String choice = input.nextLine();
            if (choice.equals("#")) {
                for (String s: ValidData.getValidSections()) {
                    if (ValidData.getValidType(s).contains(type)) {
                        String animal = exploreSection(s);
                        return exploreAnimal(animal);
                    }
                }
            } else if (Integer.parseInt(choice) > 0
                && Integer.parseInt(choice) <= ValidData.getAttraction(type).size()) {
                attraction = ValidData.getAttraction(type).get(Integer.parseInt(choice) - 1);
                selectedAttraction = new Attractions(attraction, type, checkAnimals(type));
            } else {
                return exploreAnimal(type);
            }
        } catch (Exception e) {
            return exploreAnimal(type);
        }
        return selectedAttraction;
    }

    // Method to ask user's name
    private static String askName() {
        Scanner input = new Scanner(System.in);
        System.out.print("\nWow, one more step,\nplease let us know your name: ");
        return input.nextLine();
    }

    /* Method to register user as visitor in Javari Park based on chosen attraction. This method
    return the Registration data*/
    private static Registration register(SelectedAttraction selected) {
        Scanner input = new Scanner(System.in);
        int id = RegisteredVisitor.getCounter();
        List<SelectedAttraction> selectedAttraction = new ArrayList<SelectedAttraction>();
        Registration visitor = RegisteredVisitor.findVisitor(id);
        if (visitor == null) {
            String name = askName();
            visitor = new Visitor(id, name, selectedAttraction);
        }
        visitor.addSelectedAttraction(selected);
        List<String> allAttractions = new ArrayList<String>();
        List<String> allAnimals = new ArrayList<String>();
        List<String> names = new ArrayList<String>();
        for (SelectedAttraction att: visitor.getSelectedAttractions()) {
            allAttractions.add(att.getName() + " -> " + att.getType());
        }
        for (SelectedAttraction att: visitor.getSelectedAttractions()) {
            for (Animal animal: att.getPerformers()) {
                names.add(animal.getName());
            }
        }
        for (String animalNames: names) {
            allAnimals.add(animalNames);
        }
        System.out.println("\nYeay, final check!\nHere is your data, and the attraction you chose:");
        System.out.print("\nName: " + visitor.getVisitorName());
        System.out.print("\nAttractions: " + String.join(", ", allAttractions));
        System.out.print("\nWith: ");
        if (visitor.getSelectedAttractions().size() == 1) {
            System.out.print(String.join(", ", names));
        } else {
            System.out.print(String.join(", ", allAnimals));
        }
        while (true) {
            System.out.print("\nIs the data correct? (Y/N): ");
            String answer = input.nextLine();
            if (answer.equalsIgnoreCase("y")) {
                RegisteredVisitor.getVisitors().add(visitor);
                break;
            } else if (answer.equalsIgnoreCase("n")) {
                SelectedAttraction a = exploreAnimal(selected.getType());
                return register(a);
            }
        }
        while (true) {
            System.out.print("\nThank you for your interest. Would you like to register to other "
                           + " attraction? (Y/N): ");
            String answer = input.nextLine();
            if (answer.equalsIgnoreCase("y")) {
                SelectedAttraction a = exploreAnimal(selected.getType());
                return register(a);
            } else if (answer.equalsIgnoreCase("n")) {
                RegisteredVisitor.setCounter(RegisteredVisitor.getCounter() + 1);
                break;
            }
        }
        return visitor;
    }

    public static void main(String[] args) {
        /* Main will prompt the user to put a path containing the required csv files. Then the files
        will be processed and user will be prompted to explore Javari Park*/
        Scanner input = new Scanner(System.in);
        System.out.print("Welcome to Javari Park Festival - Registration Service!\n\n... Opening "
                       + "default section database from data.");
        File defaultFile = new File(".");
        File[] defaultFiles = defaultFile.listFiles();
        if (defaultFiles != null && checkFiles(defaultFiles)) {
            System.out.println("\n\n... Loading... Success... System is populating data...");
            File[] csv = makeFiles(defaultFiles);
            Path categoryAndSection = csv[0].toPath();
            Path attraction = csv[1].toPath();
            Path record = csv[2].toPath();
            try {
                CategoriesReader category = new CategoriesReader(categoryAndSection);
                SectionsReader section = new SectionsReader(categoryAndSection);
                TypeReader type = new TypeReader(categoryAndSection);
                AttractionsReader attractions = new AttractionsReader(attraction);
                RecordsReader records = new RecordsReader(record);
                System.out.println(validData(category, section, type, attractions, records));
                records.bringAnimals();
                if (ValidData.getValidAnimals().size() == 0) {
                    System.out.println("\nWe are terribly sorry to say that Javari Park "
                                     + "is not open to public yet. Thank you for your interest");
                    System.exit(0);
                } while (true) {
                    System.out.println("\nWelcome to Javari Park Festival - Registration "
                    + "Service!\n\nPlease answer the questions by typing the number. Type #"
                    + " if you want to return to the previous menu");
                    String s = explorePark();
                    String t = exploreSection(s);
                    SelectedAttraction a = exploreAnimal(t);
                    Registration r = register(a);
                    Path p = Paths.get(".");
                    RegistrationWriter.writeJson(r, p);
                }
            } catch (IOException e) {
                System.out.print("File not found");
            }
        } else {
            while (true) {
                System.out.print(" ... File not found or incorrect file!\n\nPlease provide "
                                +"the source data path: ");
                String dataPath = input.nextLine();
                File file = new File(dataPath);
                File[] csvFiles = file.listFiles();
                if (csvFiles == null || !checkFiles(csvFiles)) {
                    System.out.print("\n");
                } else {
                    System.out.println("\n... Loading... Success... System is populating data...");
                    File[] csv = makeFiles(csvFiles);
                    Path categoryAndSection = csv[0].toPath();
                    Path attraction = csv[1].toPath();
                    Path record = csv[2].toPath();
                    try {
                        CategoriesReader category = new CategoriesReader(categoryAndSection);
                        SectionsReader section = new SectionsReader(categoryAndSection);
                        TypeReader type = new TypeReader(categoryAndSection);
                        AttractionsReader attractions = new AttractionsReader(attraction);
                        RecordsReader records = new RecordsReader(record);
                        System.out.println(validData(category, section, type, attractions, records));
                        records.bringAnimals();
                        if (ValidData.getValidAnimals().size() == 0) {
                            System.out.println("\nWe are terribly sorry to say that Javari Park "
                                + "is not open to public yet. Thank you for your interest");
                            break;
                        } while (true) {
                            System.out.println("\nWelcome to Javari Park Festival - Registration "
                            + "Service!\n\nPlease answer the questions by typing the number. Type #"
                            + " if you want to return to the previous menu");
                            String s = explorePark();
                            String t = exploreSection(s);
                            SelectedAttraction a = exploreAnimal(t);
                            Registration r = register(a);
                            Path p = Paths.get(dataPath);
                            RegistrationWriter.writeJson(r, p);
                        }
                    } catch (IOException e) {
                        System.out.print("File not found");
                    }
                }
            }
        }
    }
}