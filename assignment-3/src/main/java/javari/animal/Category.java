package javari.animal;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

public class Category {
    // Lists that store categories and its animals
    private static List<String> animals = Arrays.asList("mammals", "aves", "reptiles");
    private static List<String> mammals = Arrays.asList("Cat", "Lion", "Hamster", "Whale");
    private static List<String> aves = Arrays.asList("Eagle", "Parrot");
    private static List<String> reptiles = Arrays.asList("Snake");

    public static List<String> getList(String list) {
        List<String> output = new ArrayList<String>();
        if (list.equalsIgnoreCase("mammals")) {
            output = mammals;
        } else if (list.equalsIgnoreCase("aves")) {
            output = aves;
        } else if (list.equalsIgnoreCase("reptiles")) {
            output = reptiles;
        }
        return output;
    }

    public static List<String> getAnimals() {
        return animals;
    }
}