package javari.animal;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class Type {
    // List that stores all the types accepted in program
    private static List<String> allTypes = Arrays.asList("Cat", "Hamster", "Whale", "Lion", 
                                                         "Parrot", "Eagle", "Snake");

    public static List<String> getAllTypes() {
        return allTypes;
    }
}