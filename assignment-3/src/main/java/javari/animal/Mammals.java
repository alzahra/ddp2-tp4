package javari.animal;

import java.util.Arrays;
import java.util.List;

public class Mammals extends Animal {
    private String status; // special condition

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.status = specialStatus;
    }

    public boolean specificCondition() {
        if (this.status.equalsIgnoreCase("pregnant") || this.getType().equalsIgnoreCase("Lion")
            && this.getGender().equals(Gender.FEMALE)) {
            return false;
        }
        return true;
    }
}