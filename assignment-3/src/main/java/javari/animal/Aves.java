package javari.animal;

import java.util.Arrays;
import java.util.List;

public class Aves extends Animal {
    private String status;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.status = specialStatus;
    }

    public boolean specificCondition() {
        if (this.status.equalsIgnoreCase("laying eggs")) {
            return false;
        }
        return true;
    }
}