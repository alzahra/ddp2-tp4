package javari.animal;

import java.util.Arrays;
import java.util.List;

public class Reptiles extends Animal {
    private String status;

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                  double weight, String specialStatus, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.status = specialStatus;
    }

    public boolean specificCondition() {
        if (this.status.equalsIgnoreCase("tame")) {
            return true;
        }
        return false;
    }
}