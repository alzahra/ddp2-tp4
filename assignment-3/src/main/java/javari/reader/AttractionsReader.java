package javari.reader;

import java.util.Arrays;
import java.util.ArrayList;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import javari.reader.ValidData;
import javari.park.AttractionType;

public class AttractionsReader extends CsvReader {
    private String[][] attractionInfo = new String[this.getLines().size()][2];
    int typeIdx = 0; // type index
    int attrIdx = 1; // attraction index
    private List<String> validAttractions = new ArrayList<String>();
    private List<String> invalidAttractions = new ArrayList<String>();

    public AttractionsReader(Path file) throws IOException {
        super(file);
    }

    public void splitLines() {
        for (int i = 0; i < this.getLines().size(); i++) {
            this.attractionInfo[i] = this.getLines().get(i).split(COMMA);
        }
    }

    /* Method to count the total of valid attractions if and if only the attraction match with its
    animals and both the attraction & animal are accepted into the program*/
    public long countValidRecords() {
        this.splitLines();
        for (int i = 0; i < this.getLines().size(); i++) {
            String type = this.attractionInfo[i][typeIdx];
            String attraction = this.attractionInfo[i][attrIdx];
            if (this.find(ValidData.getValidAnimals(), type) 
                && this.find(AttractionType.getAttractions(), attraction)
                && this.find(AttractionType.getList(type), attraction)
                && !this.find(this.validAttractions, attraction)) {
                this.validAttractions.add(this.realString(AttractionType.getAttractions(), 
                                                          attraction));
            }
            if (!this.find(ValidData.getAttraction(type), attraction)) {
                ValidData.getAttraction(type).add(this.realString(AttractionType.getAttractions(),
                                                                  attraction));
            }
        }
        return (long) this.validAttractions.size();
    }

    // Method to count invalid attraction which dont match the requirements
    public long countInvalidRecords() {
        this.splitLines();
        for (int i = 0; i < this.getLines().size(); i++) {
            String type = this.attractionInfo[i][typeIdx];
            String attraction = this.attractionInfo[i][attrIdx];
            if (!this.find(ValidData.getValidAnimals(), type) 
                || !this.find(AttractionType.getAttractions(), attraction)
                || !this.find(AttractionType.getList(type), attraction)) {
                this.invalidAttractions.add(attraction);
            }
        }
        return (long) this.invalidAttractions.size();
    }
}