package javari.reader;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import javari.animal.*;

public class ValidData {
    // Lists to store all valid data acquired from CsvReader
    private static List<Animal> animals = new ArrayList<Animal>();
    private static List<String> validAnimals = new ArrayList<String>();
    private static List<String> validMammals = new ArrayList<String>();
    private static List<String> validAves = new ArrayList<String>();
    private static List<String> validReptiles = new ArrayList<String>();
    private static List<String> validSections = new ArrayList<String>();
    private static List<String> cat = new ArrayList<String>();
    private static List<String> hamster = new ArrayList<String>();
    private static List<String> lion = new ArrayList<String>();
    private static List<String> whale = new ArrayList<String>();
    private static List<String> eagle = new ArrayList<String>();
    private static List<String> parrot = new ArrayList<String>();
    private static List<String> snake = new ArrayList<String>();

    public static List<String> getAttraction(String list) {
        List<String> output = new ArrayList<String>();
        if (list.equalsIgnoreCase("cat")) {
            output = cat;
        } else if (list.equalsIgnoreCase("hamster")) {
            output = hamster;
        } else if (list.equalsIgnoreCase("lion")) {
            output = lion;
        } else if (list.equalsIgnoreCase("whale")) {
            output = whale;
        } else if (list.equalsIgnoreCase("eagle")) {
            output = eagle;
        } else if (list.equalsIgnoreCase("parrot")) {
            output = parrot;
        } else if (list.equalsIgnoreCase("snake")) {
            output = snake;
        }
        return output;
    }

    public static List<String> getValidType(String list) {
        List<String> output = new ArrayList<String>();
        if (list.equalsIgnoreCase("Explore the Mammals")) {
            output = validMammals;
        } else if (list.equalsIgnoreCase("World of Aves")) {
            output = validAves;
        } else if (list.equalsIgnoreCase("Reptilian Kingdom")) {
            output = validReptiles;
        }
        return output;
    }

    public static List<String> getValidAnimals() {
        return validAnimals;
    }

    public static List<String> getValidSections() {
        return validSections;
    }

    public static List<Animal> getAnimals() {
        return animals;
    }
}