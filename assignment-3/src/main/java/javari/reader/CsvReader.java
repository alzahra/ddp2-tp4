package javari.reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Alya Zahra. Added find and realString methods for convenience of checking
 *         whether a string is inside an array of strings regardless of string cases then
 *         return the real string from the array.
 */
public abstract class CsvReader {

    public static final String COMMA = ",";

    private final Path file;
    protected final List<String> lines;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public CsvReader(Path file) throws IOException {
        this.file = file;
        this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
    }

    /**
     * Returns all line of text from CSV file as a list.
     *
     * @return
     */
    public List<String> getLines() {
        return lines;
    }

    /**
    * Return boolean based on whether a string is in array of string regardless of letter case
    * @param list is a list of strings
    * @param str is a string that will be searched in the list
    */
    public boolean find(List<String> list, String str) {
        if (list.size() != 0) {
            for (String s: list) {
                if (s.equalsIgnoreCase(str)) {
                    return true;
                }
            }
        } else {
            return false;
        }
        return false;
    }

    /**
    * Return the real string/item from the list of strings. Return empty string if the search fails
    * @param list is a list of strings
    * @param str is a string that will be searched in the list regardless of letter case
    */
    public String realString(List<String> list, String str) {
        if (list.size() != 0) {
            for (String s: list) {
                if (s.equalsIgnoreCase(str)) {
                    return s;
                }
            }
        } else {
            return "";
        }
        return "";
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public abstract long countValidRecords();

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public abstract long countInvalidRecords();
}
