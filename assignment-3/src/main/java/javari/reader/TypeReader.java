package javari.reader;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import javari.animal.Type;
import javari.reader.CategoriesReader;
import javari.reader.SectionsReader;
import javari.animal.Category;
import javari.park.Section;
import javari.reader.ValidData;

public class TypeReader extends CsvReader {
    private String[][] fileInfo = new String[this.getLines().size()][3];
    int typeIdx = 0; // index of animal type
    int ctgryIdx = 1; // index of category
    int sectIdx = 2; // index of section

    public TypeReader(Path file) throws IOException {
        super(file);
    }

    public void splitLines() { // split file's lines
        for (int i = 0; i < this.getLines().size(); i++) {
            this.fileInfo[i] = this.getLines().get(i).split(COMMA);
        }
    }

    /* Add animal type to ValidData if and if only the program accepted the type, category,
    and section as a whole*/
    public void checkType(CategoriesReader a, SectionsReader b) {
        this.splitLines();
        for (int i = 0; i < this.getLines().size(); i++) {
            String type = this.fileInfo[i][typeIdx];
            String category = this.fileInfo[i][ctgryIdx];
            String section = this.fileInfo[i][sectIdx];
            if (a.find(a.getValidCategories(), category)
                && b.find(b.getValidSections(), section)
                && this.find(Category.getList(category), type)
                && this.find(Section.getList(section), type)
                && !this.find(ValidData.getValidAnimals(), type)) {
                ValidData.getValidAnimals().add(this.realString(Type.getAllTypes(), type));
            }
            if (!this.find(ValidData.getValidSections(), section)) {
                ValidData.getValidSections().add(this.realString(Section.getSections(), section));
            }
            if (!this.find(ValidData.getValidType(section), type)) {
                ValidData.getValidType(section).add(this.realString(Type.getAllTypes(), type));             
            }
        }
    }

    public long countValidRecords() {
        return 0;
    }

    public long countInvalidRecords() {
        return 0;
    }
}