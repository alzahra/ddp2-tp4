package javari.reader;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.lang.NumberFormatException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import javari.reader.ValidData;
import javari.animal.*;

public class RecordsReader extends CsvReader {
    private String[][] recordInfo = new String[this.getLines().size()][8];
    int idIdx = 0; // ID index
    int typeIdx = 1; // type index
    int genIdx = 3; // gender index
    int lenIdx = 4; // body length index
    int weigIdx = 5; // body weight index
    int statIdx = 7; // status index
    private List<String> validIDs = new ArrayList<String>();
    private List<String> invalidIDs = new ArrayList<String>();

    public RecordsReader(Path file) throws IOException {
        super(file);
    }

    public void splitLines() {
        for (int i = 0; i < this.getLines().size(); i++) {
            this.recordInfo[i] = this.getLines().get(i).split(COMMA);
        }
    }

    public List<String> getValidIDs() {
        return this.validIDs;
    }

    public String[][] getRecordInfo() {
        return this.recordInfo;
    }

    private boolean isInteger(String num) { // check if a number is an integer
        try {
            Integer number = Integer.parseInt(num);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private boolean isDouble(String num) { // check if a number is a double
        try {
            double number = Double.parseDouble(num);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /* Method to count the total of valid records if and if only the type is a valid type and the
    animal's attribute doesnt give any error*/
    public long countValidRecords() {
        this.splitLines();
        for (int i = 0; i < this.getLines().size(); i++) {
            String id = this.recordInfo[i][idIdx];
            String type = this.recordInfo[i][typeIdx];
            String gender = this.recordInfo[i][genIdx];
            String length = this.recordInfo[i][lenIdx];
            String weight = this.recordInfo[i][weigIdx];
            String status = this.recordInfo[i][statIdx];
            if (this.find(ValidData.getValidAnimals(), type) && isInteger(id) && isDouble(length) 
                && isDouble(weight) && (gender.equalsIgnoreCase("male") || gender.equalsIgnoreCase("female")) 
                && (status.equalsIgnoreCase("healthy") || status.equalsIgnoreCase("not healthy"))
                && !this.validIDs.contains(id)) {
                this.validIDs.add(id);
            } else {
                this.invalidIDs.add(id);
            }
        }
        return (long) this.validIDs.size();
    }

    public long countInvalidRecords() {
        return (long) this.invalidIDs.size();
    }

    // Instantiate all animals in valid records and store it in ValidData
    public void bringAnimals() {
        for (int i = 0; i < this.getLines().size(); i++) {
            if (this.getValidIDs().contains(this.recordInfo[i][0])) {
                Integer id = Integer.parseInt(this.recordInfo[i][0]);
                String type = this.recordInfo[i][1];
                String name = this.recordInfo[i][2];
                if (name.length() == 0 || name.length() == 1) {
                    name = name;
                } else {
                    name = name.substring(0, 1).toUpperCase() + name.substring(1);
                }
                Gender gender = Gender.parseGender(this.recordInfo[i][3]);
                double length = Double.parseDouble(this.recordInfo[i][4]);
                double weight = Double.parseDouble(this.recordInfo[i][5]);
                String status = this.recordInfo[i][6];
                Condition condition = Condition.parseCondition(this.recordInfo[i][7]);
                if (this.find(Category.getList("mammals"), type)) {
                    Animal mammal = new Mammals(id, type, name, gender, length, weight, status, condition);
                    ValidData.getAnimals().add(mammal);
                } else if (this.find(Category.getList("aves"), type)) {
                    Animal aves = new Aves(id, type, name, gender, length, weight, status, condition);
                    ValidData.getAnimals().add(aves);
                } else if (this.find(Category.getList("reptiles"), type)) {
                    Animal reptile = new Reptiles(id, type, name, gender, length, weight, status, condition);
                    ValidData.getAnimals().add(reptile);
                }
            }
        }
    }
}