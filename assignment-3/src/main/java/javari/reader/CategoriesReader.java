package javari.reader;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import javari.animal.Category;

public class CategoriesReader extends CsvReader {
    private String[][] fileInfo = new String[this.getLines().size()][3]; // Store file info
    int typeIdx = 0; // index of animal type
    int ctgryIdx = 1; // index of category
    private List<String> categoryValid = new ArrayList<String>(); // Store valid categories
    private List<String> categoryInvalid = new ArrayList<String>(); // Store invalid categories

    public CategoriesReader(Path file) throws IOException {
        super(file);
    }

    // Split lines in read file
    public void splitLines() {
        for (int i = 0; i < this.getLines().size(); i++) {
            this.fileInfo[i] = this.getLines().get(i).split(COMMA);
        }
    }

    public List<String> getValidCategories() {
        return this.categoryValid;
    }

    /* Method to count the total of valid categories if and if only the category matches with its
    animals and both the category & animal are accepted into the program*/
    public long countValidRecords() {
        this.splitLines();
        for (int i = 0; i < this.getLines().size(); i++) {
            String type = this.fileInfo[i][typeIdx];
            String category = this.fileInfo[i][ctgryIdx];
            if (this.find(Category.getAnimals(), category) 
                && this.find(Category.getList(category), type)
                && !this.find(this.categoryValid, category)) {
                this.categoryValid.add(this.realString(Category.getAnimals(), category));
            }
        }
        return (long) this.categoryValid.size();
    }

    /* Method to count the total of invalid categories because they dont match the requirement*/
    public long countInvalidRecords() {
        this.splitLines();
        for (int i = 0; i < this.getLines().size(); i++) {
            String type = this.fileInfo[i][typeIdx];
            String category = this.fileInfo[i][ctgryIdx];
            if (!this.find(Category.getAnimals(), category) 
                || !this.find(Category.getList(category), type)) {
                this.categoryInvalid.add(category);
            }
        }
        return (long) this.categoryInvalid.size();
    }
}