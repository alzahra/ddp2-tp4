package javari.reader;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import javari.park.Section;

public class SectionsReader extends CsvReader {
    private String[][] fileInfo = new String[this.getLines().size()][3]; // store file info
    int typeIdx = 0; // index of animal type
    int sectIdx = 2; // index of section
    private List<String> sectionValid = new ArrayList<String>(); //hold valid sections
    private List<String> sectionInvalid = new ArrayList<String>(); //hold invalid section

    public SectionsReader(Path file) throws IOException {
        super(file);
    }

    public void splitLines() { // split lines of the file
        for (int i = 0; i < this.getLines().size(); i++) {
            this.fileInfo[i] = this.getLines().get(i).split(COMMA);
        }
    }

    public List<String> getValidSections() {
        return this.sectionValid;
    }

    /* Method to count the total of valid categories if and if only the section matches with its
    animals and both the section & animal are accepted into the program*/
    public long countValidRecords() {
        this.splitLines();
        for (int i = 0; i < this.getLines().size(); i++) {
            String type = this.fileInfo[i][typeIdx];
            String section = this.fileInfo[i][sectIdx];
            if (this.find(Section.getSections(), section) 
                && this.find(Section.getList(section), type) 
                && (!this.find(this.sectionValid, section))) {
                this.sectionValid.add(this.realString(Section.getSections(), section));
            }
        }
        return (long) this.sectionValid.size();
    }

    // Method to count sections that dont match the requirements
    public long countInvalidRecords() {
        this.splitLines();
        for (int i = 0; i < this.getLines().size(); i++) {
            String type = this.fileInfo[i][typeIdx];
            String section = this.fileInfo[i][sectIdx];
            if (!this.find(Section.getSections(), section) 
                || !this.find(Section.getList(section), type)) {
                this.sectionInvalid.add(section);
            }
        }
        return (long) this.sectionInvalid.size();
    }
}