package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {
    private int registrationId; // ID of the visitor
    private String name; // Visitor's name
    private List<SelectedAttraction> attractionsList = new ArrayList<SelectedAttraction>(); // List of selected attractions

    public Visitor(int registrationId, String name, List<SelectedAttraction> attractionsList) {
        this.registrationId = registrationId;
        this.name = name;
        this.attractionsList = attractionsList;
    }

    public int getRegistrationId() {
        return this.registrationId;
    }

    public String getVisitorName() {
        return this.name;
    }

    public String setVisitorName(String name) {
        this.name = name;
        return this.name;
    }

    public List<SelectedAttraction> getSelectedAttractions() {
        return this.attractionsList;
    }

    // Add attractions to the list
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if (!this.attractionsList.contains(selected)) {
            this.attractionsList.add(selected);
            return true;
        }
        return false;
    }
}