package javari.park;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class AttractionType {
    // Lists that store accepted attractions and the attractions each animal can perform
    private static List<String> list = Arrays.asList("Circles of Fires", "Dancing Animals", "Counting Masters", "Passionate Coders");
    private static List<String> cat = Arrays.asList("Passionate Coders", "Dancing Animals");
    private static List<String> hamster = Arrays.asList("Passionate Coders", "Counting Masters", "Dancing Animals");
    private static List<String> lion = Arrays.asList("Circles of Fires");
    private static List<String> whale = Arrays.asList("Circles of Fires", "Counting Masters");
    private static List<String> eagle = Arrays.asList("Circles of Fires");
    private static List<String> parrot = Arrays.asList("Dancing Animals", "Counting Masters");
    private static List<String> snake = Arrays.asList("Dancing Animals", "Passionate Coders");

    public static List<String> getList(String list) {
        List<String> output = new ArrayList<String>();
        if (list.equalsIgnoreCase("cat")) {
            output = cat;
        } else if (list.equalsIgnoreCase("hamster")) {
            output = hamster;
        } else if (list.equalsIgnoreCase("lion")) {
            output = lion;
        } else if (list.equalsIgnoreCase("whale")) {
            output = whale;
        } else if (list.equalsIgnoreCase("eagle")) {
            output = eagle;
        } else if (list.equalsIgnoreCase("parrot")) {
            output = parrot;
        } else if (list.equalsIgnoreCase("snake")) {
            output = snake;
        }
        return output;
    }

    public static List<String> getAttractions() {
        return list;
    }
}