package javari.park;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class Section {
    // Lists that store accepted sections and its animals within
    private static List<String> sectionList = Arrays.asList("Explore the Mammals", "World of Aves", "Reptilian Kingdom");
    private static List<String> mammals = Arrays.asList("Hamster", "Lion", "Cat", "Whale");
    private static List<String> aves = Arrays.asList("Eagle", "Parrot");
    private static List<String> reptiles = Arrays.asList("Snake");

    public static List<String> getList(String list) {
        List<String> output = new ArrayList<String>();
        if (list.equalsIgnoreCase("Explore the Mammals")) {
            output = mammals;
        } else if (list.equalsIgnoreCase("World of Aves")) {
            output = aves;
        } else if (list.equalsIgnoreCase("Reptilian Kingdom")) {
            output = reptiles;
        }
        return output;
    }

    public static List<String> getSections() {
        return sectionList;
    }
}