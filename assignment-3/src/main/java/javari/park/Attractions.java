package javari.park;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import javari.animal.*;

public class Attractions implements SelectedAttraction {
    private String name; // Attraction's name
    private String type; // Animal's type at attraction
    private List<Animal> performers = new ArrayList<Animal>(); // List of animals at attraction

    public Attractions(String name, String type, List<Animal> performers) {
        this.name = name;
        this.type = type;
        this.performers = performers;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public List<Animal> getPerformers() {
        return this.performers;
    }

    // Method to add animals to perform at attraction
    public boolean addPerformer(Animal performer) {
        if (!this.performers.contains(performer)) {
            this.performers.add(performer);
            return true;
        }
        return false;
    }
}