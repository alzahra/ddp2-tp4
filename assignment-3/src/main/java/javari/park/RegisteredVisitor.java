package javari.park;

import java.util.List;
import java.util.ArrayList;

public class RegisteredVisitor {
    private static int idCounter = 1; // Counter to keep track of used IDs

    private static List<Registration> visitors = new ArrayList<Registration>(); // Store registered visitors

    public static List<Registration> getVisitors() {
        return visitors;
    }

    public static int getCounter() {
        return idCounter;
    }

    public static void setCounter(int i) {
        idCounter = i;
    }

    // Method to find visitor based on their ID, return null if not found
    public static Registration findVisitor(int id) {
        if (visitors.size() != 0) {
            for (Registration r: visitors) {
                if (id == r.getRegistrationId()) {
                    Registration visitor = r;
                    return r;
                }
            }
        }
        return null;
    }
}