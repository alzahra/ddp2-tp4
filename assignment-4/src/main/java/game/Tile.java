package game;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Paths;
import javax.swing.ImageIcon;
import javax.swing.JButton;

class Tile extends JButton {
    private int imgID; // image id of the tile
    private String directory = Paths.get("").toAbsolutePath().toString(); // current working directory
    public ImageIcon cover = new ImageIcon(directory + "\\images\\cover.jpg"); // tile's cover image

    /**
     * Constructor of class Tile.
     * @param id integer id which will be the name of the image the tile holds (ending in .jpg)
     */
	public Tile(int id) {
        setPreferredSize(new Dimension(130, 130));
        if (cover != null) {
            setIcon(cover);
        }
        imgID = id;
        addActionListener(new ChangeImage());
    }

    /**
     * Inner class that implements ActionListener interface.
     * Change the tile's image from its cover to the image it holds when clicked.
     */
    class ChangeImage implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (getIcon().equals(cover)) {
                ImageIcon image = new ImageIcon(directory + "\\images\\" + imgID + ".jpg");
                setIcon(image);
            }
        }
    }

    /**
     * Accessor of tile's image id.
     * @return tile's image id
     */
    public int getID() {
	    return imgID;
    }
}