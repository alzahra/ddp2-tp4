package game;

import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Game {
    public Board board = new Board(); // new board of game
    private JPanel mainPanel = new JPanel(); // main panel
    private JPanel panel1 = new JPanel(); // panel to hold the tiles
    private JPanel panel2 = new JPanel(); // panel to hold label and other buttons

    /**
     * A method to set up preparation for a new board.
     */
    public void newBoard() {
        this.board.setTitle("Match The Heroes!");
        this.board.setImageTotal(18);
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        panel1.setLayout(new GridLayout(6, 6));
        panel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        panel1.setPreferredSize(new Dimension(815, 815));
        panel2.setPreferredSize(new Dimension(815, 55));
        mainPanel.add(panel1);
        mainPanel.add(panel2);
        this.board.getContentPane().add(mainPanel);
    }

    /**
     * Method to reset the game.
     */
    public void newGame() {
        for (int i = 0; i < this.board.getCLICKED().length; i++) {
            this.board.getCLICKED()[i] = null;
        }
        this.board.makeIDs();
        this.board.setupTiles();
        this.panel1.removeAll();
        this.panel2.removeAll();
        for (Tile tile: this.board.getTiles()) {
            tile.addActionListener(this.board.getAction());
            this.panel1.add(tile);
        }
        this.board.setIconImage(this.board.getTiles()[0].cover.getImage());
        Font font = new Font("Arial", Font.PLAIN, 25);
        JButton exit = this.board.makeButton("EXIT", 100, 45, font);
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        JButton reset = this.board.makeButton("Play Again", 160, 45, font);
        reset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                newGame();
                board.setAttempts(0);
                board.updateAttempt();
            }
        });
        this.board.getCounter().setFont(font);
        this.panel2.add(this.board.getCounter());
        this.panel2.add(reset);
        this.panel2.add(exit);
        this.board.pack();
    }

	public static void main(String[] args) {
        Game game = new Game();
        game.newBoard();
        game.newGame();
        game.board.setVisible(true);
    }
}