package game;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

class Board extends JFrame {
    private int ATTEMPTS = 0; // total number of tries
    private int IMG_TOTAL = 0; // total number of unique images the board has
    private int matched = 0; // total number of tiles that have matched
    private Tile[] CLICKED = new Tile[2]; // holds the currently clicked tile
    private List<Integer> IDs; // list of tile's image IDs doubled
    private Tile[] tiles; // holds all the tiles
    private JLabel counter = new JLabel("Number of Tries: 0");
    private ActionListener action = new TileAction();

    /**
     * Constructor of class Board.
     */
    public Board() {
        setSize(815, 870);
        setResizable(false);
        Toolkit tool = getToolkit();
        Dimension size = tool.getScreenSize();
        setLocation((size.width / 2) - (getWidth() / 2), (size.height / 2) - (getHeight() / 2));
    }

    /**
     * Set the total unique images the board will have.
     * @param i the number of unique images
     */
    public void setImageTotal(int i) {
        IMG_TOTAL = i;
    }

    /**
     * Create a new arraylist of integers that holds every tile's image id and doubled.
     * Shuffle the list after it's created.
     */
    public void makeIDs() {
        IDs = new ArrayList<Integer>();
        for (int i = 0; i < IMG_TOTAL; i++) {
            IDs.add(i + 1);
            IDs.add(i + 1);
        }
        Collections.shuffle(IDs);
    }

    /**
     * Create tiles for the board based on arraylist IDs' content.
     */
    public void setupTiles() {
        tiles = new Tile[IMG_TOTAL * 2];
        for (int i = 0; i < tiles.length; i++) {
            tiles[i] = new Tile(IDs.get(i));
        }
    }

    /**
     * Inner class that implements ActionListener interface to be used when a tile is clicked.
     * Makes sure only a maximum of two tiles can be opened.
     * If two opened tiles have the same image id, their visibility will be turned false.
     */
    class TileAction implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if ((e.getSource().equals(CLICKED[0])) || (e.getSource().equals(CLICKED[1]))) {
                return;
            } else {
                if (CLICKED[0] == null && CLICKED[1] == null) {
                    CLICKED[0] = (Tile) e.getSource();
                } else if (CLICKED[0] != null && CLICKED[1] == null) {
                    ATTEMPTS++;
                    updateAttempt();
                    CLICKED[1] = (Tile) e.getSource();
                    checkMatch(CLICKED[0], CLICKED[1]);
                } else if (CLICKED[0] != null){
                    CLICKED[0].setIcon(CLICKED[0].cover);
                    CLICKED[1].setIcon(CLICKED[1].cover);
                    CLICKED[0] = null;
                    CLICKED[1] = null;
                    actionPerformed(e);
                }
            }
        }
    }

    /**
     * Make an button immediately with preferred size and font
     * @param text text to be displayed on the button
     * @param width width of the button
     * @param height height of the button
     * @param font font used on the text
     * @return a JButton with immediate size and font
     */
    public JButton makeButton(String text, int width, int height, Font font) {
        JButton button = new JButton(text);
        button.setPreferredSize(new Dimension(width, height));
        button.setFont(font);
        return button;
    }

    /**
     * Check if two tiles have the same image id.
     * Set both tiles' visibility to false if they match with a delay in time
     * @param a Tile one
     * @param b Tile two
     */
    public void checkMatch(Tile a, Tile b) {
        Timer timer;
        if (a.getID() == b.getID()) {
            timer = new Timer(500, new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    a.setVisible(false);
                    b.setVisible(false);
                    matched += 2;
                    if (matched == IMG_TOTAL * 2) {
                        clear();
                    }
                }
            });
            timer.setRepeats(false);
            timer.restart();
        }
    }

    /**
     * Accessor of the board's array of currently clicked tiles.
     * @return the board's CLICKED variable
     */
    public Tile[] getCLICKED() {
        return CLICKED;
    }

    /**
     * Show a popup to congrats the player after the game has been cleared.
     */
    public void clear() {
        JOptionPane.showMessageDialog(this, "You cleared the game!", "Congratulations!",
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Mutator of the board's number of tries.
     * @param i the number of attempts the ATTEMPTS will have
     */
    public void setAttempts(int i) {
        ATTEMPTS = i;
    }

    /**
     * Update the counter label.
     */
    public void updateAttempt() {
        counter.setText("Number of Tries: " + Integer.toString(ATTEMPTS) + " ");
    }

    /**
     * Accessor of the board's label of number of tries
     * @return the board's counter variable
     */
    public JLabel getCounter() {
        return counter;
    }

    /**
     * Accessor of the board's array of tiles
     * @return the board's tiles variable
     */
    public Tile[] getTiles() {
        return tiles;
    }

    /**
     * Accessor of the action listener.
     * @return TileAction() class
     */
    public ActionListener getAction() {
        return action;
    }
}