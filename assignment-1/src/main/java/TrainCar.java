public class TrainCar {
    public static final double EMPTY_WEIGHT = 20; // Dalam kilogram
    TrainCar next;
    WildCat cat;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {  
        this(cat);
        this.next = next;

    }

    // Method rekursif menghitung total berat kereta di track
    public double computeTotalWeight() {
        double totalWeight = 0.0;
        if (this.next == null) {
            totalWeight += EMPTY_WEIGHT + this.cat.weight;
        } else {
            totalWeight += EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight();
        }
        return totalWeight;
    }
    
    // Method rekursif menghitung total body mass index semua kucing di track
    public double computeTotalMassIndex() {
        double totalMassIndex = 0.0;
        if (this.next == null) {
            totalMassIndex += this.cat.computeMassIndex();
        } else {
            totalMassIndex += this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
        return totalMassIndex;
    }
    
    // Method rekursif mencetak keadaan kereta di track
    public void printCar() {
        if (this.next == null) {
            System.out.print("(" + this.cat.name + ")");
        } else {
            System.out.print("(" + this.cat.name + ")--");
            this.next.printCar();
        }
    }
}