import java.text.DecimalFormat;
import java.util.Scanner;

public class A1Station {
    private static final double THRESHOLD = 250; // Dalam kilogram
    static TrainCar recentCar; // Variable untuk menyimpan data Train terbaru

    // Method untuk menghitung total kucing yang ada di kereta
    public static int totalKucing(TrainCar recentCar) {
        int total = 0;
        if (recentCar.next == null) {
            total += 1;
        } else {
            total += 1 + totalKucing(recentCar.next);
        }
        return total;
    }

    // Method untuk memberangkatkan kereta apabila berat total sama atau melebihi THRESHOLD
    public static void trackCar(TrainCar recentCar) {
        String category = "";
        double bmIndex = recentCar.computeTotalMassIndex() / totalKucing(recentCar);
        DecimalFormat numberFormat = new DecimalFormat("#.00");
        double bodyMassIndex = Double.parseDouble(numberFormat.format(bmIndex));
        System.out.print("The train departs to Javari Park\n[LOCO]<--");
        recentCar.printCar();
        System.out.print("\nAverage mass index of all cats: " + bodyMassIndex);
        if (bodyMassIndex < 18.5) {
            category += "underweight";
        } else if (bodyMassIndex >= 18.5 && bodyMassIndex < 25) {
            category += "normal";
        } else if (bodyMassIndex >= 25 && bodyMassIndex < 30) {
            category += "overweight";
        } else if (bodyMassIndex >= 30) {
            category += "obese";
        }
        System.out.println("\nIn average, the cats in the train are *" + category + "*");
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int jumlahKucing = input.nextInt();
        input.nextLine();
        for (int i = 0; i < jumlahKucing; i++) {
            String[] ciriKucing = input.nextLine().split(",");
            WildCat kucing = new WildCat(ciriKucing[0], Double.parseDouble(ciriKucing[1]), 
                                         Double.parseDouble(ciriKucing[2]));
            if (recentCar == null) {
                recentCar = new TrainCar(kucing);
            } else {
                recentCar = new TrainCar(kucing, recentCar);
            }
            if (recentCar.computeTotalWeight() >= THRESHOLD) {
                trackCar(recentCar);
                recentCar = null; // Mengosongkan track kereta
            }
        }
        if (recentCar != null) {
            trackCar(recentCar);
        }
    }
}