public class WildCat {

    String name;
    double weight; // Dalam kilogram
    double length; // Dalam centimeter

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        double massIndex = (this.weight / ((this.length * this.length) * 0.0001));
        return massIndex;
    }
}