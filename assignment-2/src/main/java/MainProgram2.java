import animal.Animal;
import arrangecage.ArrangeCage;
import cat.Cat;
import eagle.Eagle;
import hamster.Hamster;
import indoorcage.IndoorCage;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import lion.Lion;
import outdoorcage.OutdoorCage;
import parrot.Parrot;

public class MainProgram2 {
    /* Method visitAnimal berisi interaksi antara pengunjung dengan hewan-hewan yang ada*/
    public static void visitAnimal(List<Animal> arr, String name, String animal, String choices) {
        try {
            Scanner input = new Scanner(System.in);
            for (Animal creature: arr) {
                if (creature.getName().equals(name)) {
                    System.out.println("You are visiting " + name + " (" + animal + ") now, "
                                     + "what would you like to do?\n" + choices);
                    int number = input.nextInt();
                    if (animal.equals("cat")) {
                        Cat currentCat = (Cat) creature;
                        if (number == 1) {
                            currentCat.brush();
                            return;
                        } else if (number == 2) {
                            currentCat.cuddle();
                            return;
                        } else {
                            System.out.println("You do nothing...");
                            return;
                        }
                    } else if (animal.equals("eagle")) {
                        Eagle currentEagle = (Eagle) creature;
                        if (number == 1) {
                            currentEagle.fly();
                            return;
                        } else {
                            System.out.println("You do nothing...");
                            return;
                        }
                    } else if (animal.equals("hamster")) {
                        Hamster currentHamster = (Hamster) creature;
                        if (number == 1) {
                            currentHamster.gnaw();
                            return;
                        } else if (number == 2) {
                            currentHamster.run();
                            return;
                        } else {
                            System.out.println("You do nothing...");
                            return;
                        }
                    } else if (animal.equals("lion")) {
                        Lion currentLion = (Lion) creature;
                        if (number == 1) {
                            currentLion.hunt();
                            return;
                        } else if (number == 2) {
                            currentLion.brush();
                            return;
                        } else if (number == 3) {
                            currentLion.disturb();
                            return;
                        } else {
                            System.out.println("You do nothing...");
                            return;
                        }
                    } else if (animal.equals("parrot")) {
                        Parrot currentParrot = (Parrot) creature;
                        if (number == 1) {
                            currentParrot.fly();
                            return;
                        } else if (number == 2) {
                            System.out.print("You say: ");
                            input.nextLine();
                            String sentence = input.nextLine();
                            currentParrot.converse(sentence);
                            return;
                        } else {
                            currentParrot.alone();
                            return;
                        }
                    }
                }
            }
            System.out.println("There is no " + animal + " with that name! ");
            return;
        } catch (Exception e) {
            System.out.println("Your input is invalid!");
            return;
        }
    }

    public static void main(String[] args) {
        try {
            // Membuat sekumpulan list berisi hewan-hewan
            List<Cat> cats = new ArrayList<>();
            List<Lion> lions = new ArrayList<>();
            List<Eagle> eagles = new ArrayList<>();
            List<Parrot> parrots = new ArrayList<>();
            List<Hamster> hamsters = new ArrayList<>();
            List<Animal> allAnimals = new ArrayList<>();

            Scanner input = new Scanner(System.in);
            String[] animals = {"cat", "lion", "eagle", "parrot", "hamster"};

            System.out.println("Welcome to Javari Park!\nInput the number of animals");
            for (String animal: animals) {
                System.out.println(animal + ": ");
                int totalSpecies = input.nextInt();
                if (totalSpecies == 0) {
                    continue;
                }
                System.out.println("Provide the information of " + animal + "(s):");
                input.nextLine();
                String info = input.nextLine();
                String[] splitInfo = info.split(",");
                // Menginisiasi objek berdasarkan masukan user
                for (String data: splitInfo) {
                    int index = data.indexOf("|");
                    String animalName = data.substring(0, index);
                    int bodyLength = Integer.parseInt(data.substring(index + 1));
                    switch (animal) {
                        case ("cat"):
                            Cat cat = new Cat(animalName, bodyLength);
                            cats.add(cat);
                            allAnimals.add(cat);
                            break;
                        case ("lion"):
                            Lion lion = new Lion(animalName, bodyLength);
                            lions.add(lion);
                            allAnimals.add(lion);
                            break;
                        case("eagle"):
                            Eagle eagle = new Eagle(animalName, bodyLength);
                            eagles.add(eagle);
                            allAnimals.add(eagle);
                            break;
                        case("parrot"):
                            Parrot parrot = new Parrot(animalName, bodyLength);
                            parrots.add(parrot);
                            allAnimals.add(parrot);
                            break;
                        case("hamster"):
                            Hamster hamster = new Hamster(animalName, bodyLength);
                            hamsters.add(hamster);
                            allAnimals.add(hamster);
                            break;
                        default:
                    }
                }
            }
            System.out.println("Animals have been successfully recorded!\n\n"
                             + "=============================================");
            // Mengkategorisasikan tipe cage tiap hewan
            IndoorCage.categorizeIndoor(cats);
            IndoorCage.categorizeIndoor(hamsters);
            IndoorCage.categorizeIndoor(parrots);
            OutdoorCage.categorizeOutdoor(lions);
            OutdoorCage.categorizeOutdoor(eagles);
            // Mengatur tiga level yang menampung hewan-hewan 
            ArrangeCage.cageArrangement(cats, "indoor");
            ArrangeCage.cageArrangement(lions, "outdoor");
            ArrangeCage.cageArrangement(hamsters, "indoor");
            ArrangeCage.cageArrangement(eagles, "outdoor");
            ArrangeCage.cageArrangement(parrots, "indoor");
            
            System.out.println("NUMBER OF ANIMALS:"
                             + "\ncat: " + cats.size()
                             + "\nlion: " + lions.size()
                             + "\nparrot: " + parrots.size()
                             + "\neagle: " + eagles.size()
                             + "\nhamster: " + hamsters.size()
                             + "\n\n=============================================");

            while (true) {
                System.out.println("Which animal do you want to visit?\n"
                                 + "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
                int num = input.nextInt();
                String animal = "";
                String choices = "";
                // Membuat sekumpulan pilihan tergantung hewan yang dikunjungi
                switch (num) {
                    case 1:
                        animal += "cat";
                        choices += "1: Brush the fur 2: Cuddle";
                        break;
                    case 2:
                        animal += "eagle";
                        choices += "1: Order to fly";
                        break;
                    case 3:
                        animal += "hamster";
                        choices += "1: See it gnawing 2: Order to run in the hamster wheel";
                        break;
                    case 4:
                        animal += "parrot";
                        choices += "1: Order to fly 2: Do conversation";
                        break;
                    case 5:
                        animal += "lion";
                        choices += "1: See it hunting 2: Brush the mane 3: Disturb it";
                        break;
                    case 99:
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Option doesn't exist\n");
                        continue;
                }
                System.out.println("Mention the name of " + animal + " you want to visit: ");
                input.nextLine();
                String name = input.nextLine();
                // Memanggil method untuk berinteraksi dengan hewan tersebut
                visitAnimal(allAnimals, name, animal, choices);
                System.out.println("Back to the office!\n");
            }
        } catch (Exception e) {
            System.out.println("Your input is invalid!");
            main(null);
        }
    }
}