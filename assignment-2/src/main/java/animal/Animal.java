package animal;

public class Animal {
    protected String name;
    protected int length;
    protected String cage = "?";
    
    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
    }
    
    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }

    public String getCage() {
        return this.cage;
    }

    public void setCage(String cage) {
        this.cage = cage;
    }
}