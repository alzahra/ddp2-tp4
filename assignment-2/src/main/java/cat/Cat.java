package cat;

import animal.Animal;
import java.util.Random;

public class Cat extends Animal {
    public String[] catNoises = {"Miaaaw..", "Purrr..","Mwaw!", "Mraaawr!"};

    public Cat(String name, int length) {
        super(name, length);
    }

    // Method untuk menyisiri kucing
    public void brush() {
        System.out.println("Time to clean " + this.name + "'s fur\n"
                         + this.name + " makes a voice: Nyaaan...");
    }

    // Method untuk memeluk kucing yang akan mengeluarkan suara random
    public void cuddle() {
        int random = new Random().nextInt(catNoises.length);
        System.out.println(this.name + " makes a voice: " + catNoises[random]);
    }
}