package outdoorcage;

import animal.Animal;
import java.util.List;

public class OutdoorCage {
    // Menentukan tipe cage untuk tiap hewan outdoor/buas (lion & eagle)
    public static void categorizeOutdoor(List<? extends Animal> animal) {
        for (int i = 0; i < animal.size(); i++) {
            if (animal.get(i).getLength() < 75) {
                animal.get(i).setCage("A");
            } else if (animal.get(i).getLength() >= 75 && animal.get(i).getLength() <= 90) {
                animal.get(i).setCage("B");
            } else {
                animal.get(i).setCage("C");
            }
        }
    }
}