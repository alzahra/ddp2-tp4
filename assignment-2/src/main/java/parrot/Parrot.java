package parrot;

import animal.Animal;

public class Parrot extends Animal {
    public Parrot(String name, int length) {
        super(name, length);
    }

    // Method untuk melihat burung beo terbang
    public void fly() {
        System.out.println("Parrot " + this.name + " flies!\n"
                         + this.name + " makes a voice: FLYYY...");
    }

    // Method untuk berbicara dengan burung beo
    public void converse(String string) {
        System.out.println(this.name + " says: " + string.toUpperCase());
    }

    // Method yang dipanggil apabila visitor tidak berinteraksi dengan burung beo
    public void alone() {
        System.out.println(this.name + " says: HM?");
    }
}