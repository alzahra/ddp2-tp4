package arrangecage;

import animal.Animal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrangeCage {
    /* Method cageRearrangement digunakan untuk mengubah urutan tiap level dan me-reverse isi
    dari tiap level tersebut*/
    public static void cageRearrangement(String[] lvl1, String[] lvl2, String[] lvl3) {
        System.out.println("\nAfter rearrangement...");
        String[] level1 = Arrays.copyOf(lvl3, lvl3.length);
        String[] level2 = Arrays.copyOf(lvl1, lvl1.length);
        String[] level3 = Arrays.copyOf(lvl2, lvl2.length);
        // Reverse isi level1
        String firstlvl = "";
        if (level1.length == 0) {
            firstlvl = firstlvl;
        } else {
            for (int i = level1.length - 1; i > -1; i--) {
                firstlvl += (level1[i] + ", ");
            }
        }
        // Reverse isi level2
        String secondlvl = "";
        if (level2.length == 0) {
            secondlvl = secondlvl;
        } else {
            for (int i = level2.length - 1; i > -1; i--) {
                secondlvl += (level2[i] + ", ");
            }
        }
        // Reverse isi level3
        String thirdlvl = "";
        if (level3.length == 0) {
            thirdlvl = thirdlvl;
        } else {
            for (int i = level3.length - 1; i > -1; i--) {
                thirdlvl += (level3[i] + ", ");
            }
        }
        // Mencetak ketiga level setelah di-rearrange
        System.out.println("level 3: " + thirdlvl + "\nlevel 2: " + secondlvl + "\nlevel 1: "
                         + firstlvl + "\n");
    }

    /* Method cageArrangement memasukkan input tiap spesies hewan ke dalam tiga level tergantung
    banyaknya jumlah tiap spesies*/
    public static void cageArrangement(List<? extends Animal> array, String location) {
        if (array.size() == 0) {
            return;
        } else {
            System.out.println("Cage arrangement:\n" + "location: " + location);
            // Menentukan ukuran tiap level
            int div = array.size() / 3;
            int mod = array.size() % 3;
            int lvl1 = 0;
            int lvl2 = 0;
            int lvl3 = 0;
            if (array.size() == 1) {
                lvl1 = 1;
                lvl2 = 0;
                lvl3 = 0;
            } else if (array.size() == 2) {
                lvl1 = 1;
                lvl2 = 1;
                lvl3 = 0;
            } else {
                if (mod == 0) {
                    lvl1 = div;
                    lvl2 = div;
                    lvl3 = div;
                } else if (mod == 1) {
                    lvl1 = div;
                    lvl2 = div;
                    lvl3 = div + 1;
                } else if (mod == 2) {
                    lvl1 = div;
                    lvl2 = div + 1;
                    lvl3 = div + 1;
                }
            }
            // Menciptakan tiga array of string berbeda untuk tiap level
            String [] level1 = new String[lvl1];
            String[] level2 = new String[lvl2];
            String[] level3 = new String[lvl3];
            // Memasukkan hewan (string) ke level 1
            String firstLevel = "";
            if (level1.length == 0) {
                firstLevel = firstLevel;
            } else {
                for (int i = 0; i < lvl1; i++) {
                    level1[i] = (array.get(i).getName() + " (" + array.get(i).getLength() + " - "
                               + array.get(i).getCage() + ")");
                }
                for (String animal: level1) {
                    firstLevel += (animal + ", ");
                }
            }
            // Memasukkan hewan (string) ke level 2
            String secondLevel = "";
            if (level2.length == 0) {
                secondLevel = secondLevel;
            } else {
                for (int i = lvl1, j = 0; i < (lvl1 + lvl2) && j < lvl2; i++, j++) {
                    level2[j] = (array.get(i).getName() + " (" + array.get(i).getLength() + " - "
                               + array.get(i).getCage() + ")");
                }
                for (String animal: level2) {
                    secondLevel += (animal + ", ");
                }
            }
            // Memasukkan sisa hewan (string) ke level 3
            String thirdLevel = "";
            if (level3.length == 0) {
                thirdLevel = thirdLevel;
            } else {
                for (int i = (lvl1 + lvl2), j = 0; i < (lvl1 + lvl2 + lvl3) && j < lvl3; i++, j++) {
                    level3[j] = (array.get(i).getName() + " (" + array.get(i).getLength() + " - "
                               + array.get(i).getCage() + ")");
                }
                for (String animal: level3) {
                    thirdLevel += (animal + ", ");
                }
            }
            // Mencetak cage arrangement yang pertama
            System.out.println("level 3: " + thirdLevel + "\nlevel 2: " + secondLevel
                             + "\nlevel 1: " + firstLevel);

            cageRearrangement(level1, level2, level3); // Re-arrange cage
        }
    }
}