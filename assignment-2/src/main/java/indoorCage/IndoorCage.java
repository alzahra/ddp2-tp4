package indoorcage;

import animal.Animal;
import java.util.List;

public class IndoorCage {
    // Menentukan tipe cage untuk tiap hewan indoor/pet (cat, hamster, and parrot)
    public static void categorizeIndoor(List<? extends Animal> animal) {
        for (int i = 0; i < animal.size(); i++) {
            if (animal.get(i).getLength() < 45) {
                animal.get(i).setCage("A");
            } else if (animal.get(i).getLength() >= 45 && animal.get(i).getLength() <= 60) {
                animal.get(i).setCage("B");
            } else {
                animal.get(i).setCage("C");
            }
        }
    }
}