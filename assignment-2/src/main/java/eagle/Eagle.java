package eagle;

import animal.Animal;

public class Eagle extends Animal {
    public Eagle(String name, int length) {
        super(name, length);
    }

    // Method untuk melihat elang terbang
    public void fly() {
        System.out.println(this.name + " makes a voice: Kwaakk...\n" + "You hurt!");
    }
}