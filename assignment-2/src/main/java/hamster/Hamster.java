package hamster;

import animal.Animal;

public class Hamster extends Animal {
    public Hamster(String name, int length) {
        super(name, length);
    }

    // Method untuk melihat hamster mengunyah
    public void gnaw() {
        System.out.println(this.name + " makes a voice: Ngkkrit.. Ngkkrrriiit");
    }

    /// Method untuk melihat hamster berlari
    public void run() {
        System.out.println(this.name + " makes a voice: Trrr... Trrr...");
    }
}