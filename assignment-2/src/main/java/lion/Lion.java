package lion;

import animal.Animal;

public class Lion extends Animal {
    public Lion(String name, int length) {
        super(name, length);
    }

    // Method untuk melihat singa berburu
    public void hunt() {
        System.out.println("Lion is hunting...\n" + this.name + " makes a voice: Err...");
    }

    // Method untuk menyisir singa
    public void brush() {
        System.out.println("Clean the lion's mane...\n" + this.name + " makes a voice: Hauhhmm!");
    }

    // Method untuk mengganggu singa
    public void disturb() {
        System.out.println(this.name + " makes a voice: HAUHHMM!");
    }
}